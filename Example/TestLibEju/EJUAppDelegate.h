//
//  EJUAppDelegate.h
//  TestLibEju
//
//  Created by faneyoung@126.com on 10/15/2019.
//  Copyright (c) 2019 faneyoung@126.com. All rights reserved.
//

@import UIKit;

@interface EJUAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
