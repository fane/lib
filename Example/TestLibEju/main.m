//
//  main.m
//  TestLibEju
//
//  Created by faneyoung@126.com on 10/15/2019.
//  Copyright (c) 2019 faneyoung@126.com. All rights reserved.
//

@import UIKit;
#import "EJUAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EJUAppDelegate class]));
    }
}
